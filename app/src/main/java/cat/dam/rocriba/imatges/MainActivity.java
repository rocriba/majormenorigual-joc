package cat.dam.rocriba.imatges;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creació del RANDOM
        final int min = 1;
        final int max = 12;
        final int random_lleo = new Random().nextInt((max - min) + 1) + min;
        final int random_elefant = new Random().nextInt((max - min) + 1) + min;


        //Afegir cada imatge en un arrayList per cada cas

        ArrayList<ImageView> array_lleo = new ArrayList<ImageView>();
        array_lleo.add(findViewById(R.id.iv_lleo));
        array_lleo.add(findViewById(R.id.iv_lleo2));
        array_lleo.add(findViewById(R.id.iv_lleo3));
        array_lleo.add(findViewById(R.id.iv_lleo4));
        array_lleo.add(findViewById(R.id.iv_lleo5));
        array_lleo.add(findViewById(R.id.iv_lleo6));
        array_lleo.add(findViewById(R.id.iv_lleo7));
        array_lleo.add(findViewById(R.id.iv_lleo8));
        array_lleo.add(findViewById(R.id.iv_lleo9));
        array_lleo.add(findViewById(R.id.iv_lleo10));
        array_lleo.add(findViewById(R.id.iv_lleo11));
        array_lleo.add(findViewById(R.id.iv_lleo12));
        ArrayList<ImageView> array_elefant = new ArrayList<ImageView>();
        array_elefant.add(findViewById(R.id.iv_elefant));
        array_elefant.add(findViewById(R.id.iv_elefant2));
        array_elefant.add(findViewById(R.id.iv_elefant3));
        array_elefant.add(findViewById(R.id.iv_elefant4));
        array_elefant.add(findViewById(R.id.iv_elefant5));
        array_elefant.add(findViewById(R.id.iv_elefant6));
        array_elefant.add(findViewById(R.id.iv_elefant7));
        array_elefant.add(findViewById(R.id.iv_elefant8));
        array_elefant.add(findViewById(R.id.iv_elefant9));
        array_elefant.add(findViewById(R.id.iv_elefant10));
        array_elefant.add(findViewById(R.id.iv_elefant11));
        array_elefant.add(findViewById(R.id.iv_elefant12));

        //Posar invisible segons el random
        ImageView imatge_lleo;
        ImageView imatge_elefant;

        int i = 0;
        while (i <= random_lleo ) {

            imatge_lleo = array_lleo.get(i);
            imatge_lleo.setVisibility(View.INVISIBLE);

            i++;

        }

        int j = 0;
        while ( j <= random_elefant) {

            imatge_elefant = array_elefant.get(j);
            imatge_elefant.setVisibility(View.INVISIBLE);

            j++;

        }

        //Segons el boto que cliqui, mostrar text per pantalla

        final ImageButton ib_menor = (ImageButton) findViewById(R.id.ib_less);
        final ImageButton ib_major = (ImageButton) findViewById(R.id.ib_more);
        final ImageButton ib_igual = (ImageButton) findViewById(R.id.ib_igual);
       ib_igual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TextView tv_1 = (TextView) findViewById(R.id.tv_1);
                if ( random_lleo == random_elefant) {
                    tv_1.setText("Correcte, ho has endivinat!!");
                } else {
                    tv_1.setText("Casi, torna-ho a provar!!");
                }


            }
        });

        ib_menor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TextView tv_1 = (TextView) findViewById(R.id.tv_1);
                if ( random_elefant < random_lleo){
                    tv_1.setText("Correcte, ho has endivinat!!!");
                } else {
                    tv_1.setText("Casi, torna-ho a intentar!!!");
                }

            }
        });
        ib_major.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TextView tv_1 = (TextView) findViewById(R.id.tv_1);
                if (random_elefant > random_lleo ){
                    tv_1.setText("Correcte, ho has endivinat!!!");
                } else {
                    tv_1.setText("Casi, torna-ho a intentar!!!");
                }


            }
        });
    }
}